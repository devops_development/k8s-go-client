kubectl config set-context --current --namespace default

kubectl get job --all-namespaces
kubectl logs test-job-clientgo-mqxwj -n default
kubectl delete job test-job-clientgo -n default

kubectl config view --minify

kubectl get cronjob/test-cronjob-clientgo