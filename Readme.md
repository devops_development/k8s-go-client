# client-go

Based on  [create-and-manage-kubernetes-jobs-in-go-with-client-go-api](https://www.vultr.com/docs/create-and-manage-kubernetes-jobs-in-go-with-client-go-api/)

Go clients for talking to a [kubernetes](http://kubernetes.io/) cluster.

We recommend using the `v0.x.y` tags for Kubernetes releases >= `v1.17.0` and
`kubernetes-1.x.y` tags for Kubernetes releases < `v1.17.0`.


The Kubernetes API Server provides a REST API interface for clients to interact with it and execute operations such as creating Deployments. But a client application does not have to understand and use the lower-level HTTP operations such as GET, PUT, POST, or PATCH because there are multiple client libraries for different languages. One of the most popular ones is the Go client library.

