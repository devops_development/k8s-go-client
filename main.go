package main

import (
	"context"

	"flag"

	"fmt"

	"log"

	"path/filepath"

	batchv1 "k8s.io/api/batch/v1"

	apiv1 "k8s.io/api/core/v1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/client-go/kubernetes"

	"k8s.io/client-go/tools/clientcmd"

	"k8s.io/client-go/util/homedir"
)

var clientset *kubernetes.Clientset

const jobName = "test-job-clientgo"

const cronJobName = "test-cronjob-clientgo"

func createCronJob() {

	fmt.Println("creating cron job", cronJobName)

	cronJobsClient := clientset.BatchV1().CronJobs(apiv1.NamespaceDefault)

	cronJob := &batchv1.CronJob{ObjectMeta: metav1.ObjectMeta{

		Name: cronJobName},

		Spec: batchv1.CronJobSpec{

			Schedule: "* * * * *",

			JobTemplate: batchv1.JobTemplateSpec{

				Spec: batchv1.JobSpec{

					Template: apiv1.PodTemplateSpec{

						Spec: apiv1.PodSpec{

							Containers: []apiv1.Container{

								{

									Name: "hello",

									Image: "busybox:1.28",

									Command: []string{"/bin/sh", "-c", "date; echo Hello from the Kubernetes cluster"},
								},
							},

							RestartPolicy: apiv1.RestartPolicyOnFailure,
						},
					},
				},
			},
		},
	}

	_, err := cronJobsClient.Create(context.Background(), cronJob, metav1.CreateOptions{})

	if err != nil {

		log.Fatal("failed to create cron job", err)

	}

	fmt.Println("created cron job successfully")

}

func createJob() {

	fmt.Println("creating job", jobName)

	jobsClient := clientset.BatchV1().Jobs(apiv1.NamespaceDefault)

	job := &batchv1.Job{ObjectMeta: metav1.ObjectMeta{

		Name: jobName,
	},

		Spec: batchv1.JobSpec{

			Template: apiv1.PodTemplateSpec{

				Spec: apiv1.PodSpec{

					Containers: []apiv1.Container{

						{

							Name: "pi",

							Image: "perl:5.34.0",

							Command: []string{"perl", "-Mbignum=bpi", "-wle", "print bpi(2000)"},
						},
					},

					RestartPolicy: apiv1.RestartPolicyNever,
				},
			},
		},
	}

	_, err := jobsClient.Create(context.Background(), job, metav1.CreateOptions{})

	if err != nil {

		log.Fatal("failed to create job", err)

	}

	fmt.Println("created job successfully")

}

func deleteJob() {

	fmt.Println("deleting job", jobName)

	jobsClient := clientset.BatchV1().Jobs(apiv1.NamespaceDefault)

	pp := metav1.DeletePropagationBackground

	err := jobsClient.Delete(context.Background(), jobName, metav1.DeleteOptions{PropagationPolicy: &pp})

	if err != nil {

		log.Fatal("failed to delete job", err)

	}

	fmt.Println("deleted job successfully")

}

func deleteCronJob() {

	fmt.Println("deleting cron job", cronJobName)

	cronJobsClient := clientset.BatchV1().CronJobs(apiv1.NamespaceDefault)

	pp := metav1.DeletePropagationBackground

	err := cronJobsClient.Delete(context.Background(), cronJobName, metav1.DeleteOptions{PropagationPolicy: &pp})

	if err != nil {

		log.Fatal("failed to delete cron job", err)

	}

	fmt.Println("deleted cron job successfully")

}

func init() {

	var kubeconfig *string

	if home := homedir.HomeDir(); home != "" {

		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")

	} else {

		kubeconfig = flag.String("kubeconfig", "  ", "absolute path to the kubeconfig file")

	}

	flag.Parse()

	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)

	if err != nil {

		log.Fatal(err)

	}

	clientset, err = kubernetes.NewForConfig(config)

	if err != nil {

		log.Fatal(err)

	}

}

func main() {

	// createJob()
	// deleteJob()
	// createCronJob()
	deleteCronJob()

}
